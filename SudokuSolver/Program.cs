﻿using SudokuSolver.Strategies;
using SudokuSolver.Workers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuSolver
{
    class Program
    {
        

        static void Main(string[] args)
        {
            try
            {
                SudokuMapper sudokuMapper = new SudokuMapper();
                SudokuBoardStateManager sudokuBoardStateManager = new SudokuBoardStateManager();
                SudokuSolverEngine sudokuSolverEngine = new SudokuSolverEngine(sudokuBoardStateManager, sudokuMapper);
                SudokuFileReader sudokuFileReader = new SudokuFileReader();
                SudokuBoardDisplayer sudokuBoardDisplayer = new SudokuBoardDisplayer();

                Console.WriteLine("\nEnter filename of Sudoku Puzzle:");
                var filename = Console.ReadLine();

                var sudokuBoard = sudokuFileReader.ReadFile(filename);
                sudokuBoardDisplayer.Display("Initial state.", sudokuBoard);

                Console.WriteLine("Press a key to start solving");

                Console.ReadLine();
                Console.Clear();

                bool isSudokuSolved = sudokuSolverEngine.Solve(sudokuBoard);
                sudokuBoardDisplayer.Display("Final State.", sudokuBoard);

                Console.WriteLine(isSudokuSolved ? "Sudoku Successfully Solved!" : "Sudoku Strategy Failed!!!!");

               
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} {1}", "Sudoku Puzzle Error ", ex.Message);
            }

            Console.WriteLine("Press a key to continue");
            Console.ReadLine();


        }
    }
}
